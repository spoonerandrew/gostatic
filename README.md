# GoStatic

Small binary for serving html from a ./static folder on static port 5000

# To Use

```
wget https://gitlab.com/spoonerandrew/gostatic/-/jobs/artifacts/main/download?job=compile -O artifacts.zip
unzip artifacts.zip
mkdir static
cat > static/index.html << EOF
<html><body><p>Spooner Woz Ere</p></body></html>
EOF
./server
```

# Why would I use this?

Well I am not going to stop you, but it was mostly built for me.

# Why is this public?

So I can use it without having to authenticate.
